using Microsoft.EntityFrameworkCore;
using lab3.Models;
using Microsoft.Extensions.Configuration;

namespace laba3
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Calls> Calls { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Person> Persons { get; set; }
        private string connectionString = null;

        public DatabaseContext()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                
            this.connectionString = config.GetConnectionString("test");
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(this.connectionString);
        }
    }
}